package cat.itb.DAO;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import cat.itb.model.SobreModelDAO;
@Dao
public interface SobreDAO {
    @Insert
    public void insert(SobreModelDAO sobreMD);

    @Query("SELECT * FROM sobres")
    public LiveData<List<SobreModelDAO>> getSobres();
}
