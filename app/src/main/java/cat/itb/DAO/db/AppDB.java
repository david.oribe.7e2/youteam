package cat.itb.DAO.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import cat.itb.DAO.SobreDAO;
import cat.itb.model.SobreModelDAO;

@Database(entities = {SobreModelDAO.class}, version = 2, exportSchema = false)
public abstract class AppDB extends RoomDatabase {

    private static volatile AppDB INSTANCE;


    public static AppDB getINSTANCE(Context context){
        if (INSTANCE==null){
            synchronized (AppDB.class){
                if (INSTANCE==null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDB.class, "sobres_database").build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract SobreDAO getDAO();
}
