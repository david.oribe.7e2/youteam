package cat.itb.model;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity (tableName = "sobres")
public class SobreModelDAO {
    @PrimaryKey (autoGenerate = true)
    public int id;
    String namePlayer;
    String tipoSobre;



    public SobreModelDAO(String tipoSobre, String namePlayer) {
        this.tipoSobre = tipoSobre;
        this.namePlayer =namePlayer;
    }
    public String getNamePlayer() {
        return namePlayer;
    }

    public void setNamePlayer(String namePlayer) {
        this.namePlayer = namePlayer;
    }

    public String getTipoSobre() {
        return tipoSobre;
    }

    public void setTipoSobre(String tipoSobre) {
        this.tipoSobre = tipoSobre;
    }

}
