package cat.itb.model;

public class Sobre {
    String tipoSobre;

    public Sobre(String tipoSobre) {
        this.tipoSobre = tipoSobre;

    }


    public String getTipoSobre() {
        return tipoSobre;
    }

    public void setTipoSobre(String tipoSobre) {
        this.tipoSobre = tipoSobre;
    }

}
