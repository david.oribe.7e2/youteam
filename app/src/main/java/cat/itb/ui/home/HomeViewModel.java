package cat.itb.ui.home;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import cat.itb.model.SobreModelDAO;
import cat.itb.repository.SobreRepository;

public class HomeViewModel extends AndroidViewModel {

    private MutableLiveData<String> mText;
    private SobreRepository sobreRepository;
    private LiveData<List<SobreModelDAO>> sobresMD;

    public HomeViewModel(@NonNull Application application) {
        super(application);
        sobreRepository = new SobreRepository(application);
        sobresMD=sobreRepository.getSobres();
        mText = new MutableLiveData<>();
        mText.setValue("ABRE UN SOBRE!");

    }

    public LiveData<String> getText() {
        return mText;
    }


    public void saveSobre(SobreModelDAO sobreMD){
        sobreRepository.saveExcursion(sobreMD);
    }

}