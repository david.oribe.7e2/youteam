package cat.itb.ui.home;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.R;
import cat.itb.databaseSQLite.CreatedDBAccess;
import cat.itb.databaseSQLite.DatabaseAccess;
import cat.itb.databaseSQLite.DatabaseCreatedHelper;
import cat.itb.model.Sobre;
import cat.itb.model.SobreModelDAO;

public class HomeFragment extends Fragment {

    @BindView(R.id.sobreOroImage)
    ImageView sobreOroImage;
    @BindView(R.id.sobrePlataImage)
    ImageView sobrePlataImage;
    @BindView(R.id.sobreBronceImage)
    ImageView sobreBronceImage;
    @BindView(R.id.text_home)
    TextView homeText;
    @BindView(R.id.playerName2)
    TextView selectedPlayer1;
    @BindView(R.id.playerName1)
    TextView selectedPlayer2;
    @BindView(R.id.playerName3)
    TextView selectedPlayer3;
    @BindView(R.id.cartaOro1)
    ImageView cartaOro1;
    @BindView(R.id.cartaOro2)
    ImageView cartaOro2;
    @BindView(R.id.cartaOro3)
    ImageView cartaOro3;
    @BindView(R.id.cartaPlata1)
    ImageView cartaPlata1;
    @BindView(R.id.cartaPlata2)
    ImageView cartaPlata2;
    @BindView(R.id.cartaPlata3)
    ImageView cartaPlata3;
    @BindView(R.id.cartaBronce1)
    ImageView cartaBronce1;
    @BindView(R.id.cartaBronce2)
    ImageView cartaBronce2;
    @BindView(R.id.cartaBronce3)
    ImageView cartaBronce3;

    private HomeViewModel homeViewModel;

    public void hideCards(ImageView card1, ImageView card2, ImageView card3, TextView name1, TextView name2, TextView name3) {
        card1.setVisibility(View.INVISIBLE);
        card2.setVisibility(View.INVISIBLE);
        card3.setVisibility(View.INVISIBLE);

        name1.setVisibility(View.INVISIBLE);
        name2.setVisibility(View.INVISIBLE);
        name3.setVisibility(View.INVISIBLE);
    }

    public void showCards(ImageView card1, ImageView card2, ImageView card3, TextView name1, TextView name2, TextView name3) {
        card1.setVisibility(View.VISIBLE);
        card2.setVisibility(View.VISIBLE);
        card3.setVisibility(View.VISIBLE);

        name1.setVisibility(View.VISIBLE);
        name2.setVisibility(View.VISIBLE);
        name3.setVisibility(View.VISIBLE);
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        hideCards(cartaOro1, cartaOro2, cartaOro3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
        hideCards(cartaPlata1, cartaPlata2, cartaPlata3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
        hideCards(cartaBronce1, cartaBronce2, cartaBronce3, selectedPlayer1, selectedPlayer2, selectedPlayer3);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    public void setCartas(String name1, String name2, String name3) {
        selectedPlayer1.setText(name1);
        selectedPlayer2.setText(name2);
        selectedPlayer3.setText(name3);
    }

    public List<Integer> abrirSobre(Sobre sobre){
        List<Integer> selectedList = new ArrayList<>();
        if (sobre.getTipoSobre().equals("oro")){
            selectedList = chooseGoldPlayers();
        }
        if (sobre.getTipoSobre().equals("plata")){
            selectedList = chooseSilverPlayers();
        }
        if (sobre.getTipoSobre().equals("bronce")){
            selectedList = chooseBronzePlayers();
        }
        return selectedList;
    }

    private List<Integer> chooseBronzePlayers() {
        List<Integer> lista = Arrays.asList(340, 2533, 2598, 3162, 1561, 2045, 7762, 8691); //cambiar números a jugadores de bronce
        List<Integer> jugadoresSeleccionados = new ArrayList<>();
        for (int i = 0; i < 3; i++ ){
            Random aleatorio = new Random();
            jugadoresSeleccionados.add(lista.get(aleatorio.nextInt(lista.size())));

        }
        return jugadoresSeleccionados;
    }

    private List<Integer> chooseSilverPlayers() {
        List<Integer> lista = Arrays.asList(2317, 2780, 5051, 809, 1251, 1921, 7765); // cambiar números a jugadores de plata
        List<Integer> jugadoresSeleccionados = new ArrayList<>();
        for (int i = 0; i < 3; i++ ){
            Random aleatorio = new Random();
            jugadoresSeleccionados.add(lista.get(aleatorio.nextInt(lista.size())));

        }
        return jugadoresSeleccionados;
    }

    private List<Integer> chooseGoldPlayers(){
        List<Integer> lista = Arrays.asList(1995, 6176, 7867, 9039, 5737, 2340, 9699, 10673, 6377, 4142);
        List<Integer> jugadoresSeleccionados = new ArrayList<>();
        for (int i = 0; i < 3; i++ ){
            Random aleatorio = new Random();
            jugadoresSeleccionados.add(lista.get(aleatorio.nextInt(lista.size())));

        }
        return jugadoresSeleccionados;
    }
    public void clickedPack(String packType) {
        Sobre sobre = new Sobre(packType);
        List<Integer> selectedPlayers = abrirSobre(sobre);
        //sobreBronceImage.setVisibility(view.INVISIBLE);
        DatabaseAccess dbAccess = DatabaseAccess.getInstance(getContext());
        dbAccess.open();

        String player1 = dbAccess.getPlayer(selectedPlayers.get(0));
        String player2 = dbAccess.getPlayer(selectedPlayers.get(1));
        String player3 = dbAccess.getPlayer(selectedPlayers.get(2));

        setCartas(player1, player2, player3);
        dbAccess.close();
    }

    @OnClick({R.id.sobreOroImage, R.id.sobrePlataImage, R.id.sobreBronceImage, R.id.cartaOro1, R.id.cartaOro2,
            R.id.cartaOro3, R.id.cartaPlata1, R.id.cartaPlata2, R.id.cartaPlata3, R.id.cartaBronce1, R.id.cartaBronce2, R.id.cartaBronce3})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sobreOroImage:
                homeText.setText("ESCOJE UN JUGADOR");
                clickedPack("oro");
                hideSobres();
                showCards(cartaOro1, cartaOro2, cartaOro3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                break;
            case R.id.sobrePlataImage:
                homeText.setText("ESCOJE UN JUGADOR");
                clickedPack("plata");
                hideSobres();
                showCards(cartaPlata1, cartaPlata2, cartaPlata3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                break;
            case R.id.sobreBronceImage:
                homeText.setText("ESCOJE UN JUGADOR");
                clickedPack("bronce");
                hideSobres();
                showCards(cartaBronce1, cartaBronce2, cartaBronce3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                break;
            case R.id.cartaOro1:
                insertPlayerOnDB("oro", selectedPlayer1.getText().toString());
                SobreModelDAO sobreModelDAO = new SobreModelDAO("oro", selectedPlayer1.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO);
                hideCards(cartaOro1, cartaOro2, cartaOro3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                //AL CLICAR EN LA CARTA SE DEBERÍA DE GUARDAR EN UNA BASE DE DATOS PARA LUEGO MOSTRAR LOS JUGADORES QUE TENEMOS EN LA PLANTILLA
                showSobres();
                break;
            case R.id.cartaOro2:
                insertPlayerOnDB("oro", selectedPlayer2.getText().toString());
                SobreModelDAO sobreModelDAO2 = new SobreModelDAO("oro", selectedPlayer2.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO2);
                hideCards(cartaOro1, cartaOro2, cartaOro3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaOro3:
                insertPlayerOnDB("oro", selectedPlayer3.getText().toString());
                SobreModelDAO sobreModelDAO3 = new SobreModelDAO("oro", selectedPlayer3.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO3);
                hideCards(cartaOro1, cartaOro2, cartaOro3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaPlata1:
                insertPlayerOnDB("plata", selectedPlayer1.getText().toString());
                SobreModelDAO sobreModelDAO4 = new SobreModelDAO("plata", selectedPlayer1.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO4);
                hideCards(cartaPlata1, cartaPlata2, cartaPlata3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaPlata2:
                insertPlayerOnDB("plata", selectedPlayer2.getText().toString());
                SobreModelDAO sobreModelDAO5 = new SobreModelDAO("plata", selectedPlayer2.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO5);
                hideCards(cartaPlata1, cartaPlata2, cartaPlata3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaPlata3:
                insertPlayerOnDB("plata", selectedPlayer3.getText().toString());
                SobreModelDAO sobreModelDAO6 = new SobreModelDAO("plata", selectedPlayer3.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO6);
                hideCards(cartaPlata1, cartaPlata2, cartaPlata3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaBronce1:
                insertPlayerOnDB("bronce", selectedPlayer1.getText().toString());
                SobreModelDAO sobreModelDAO7 = new SobreModelDAO("bronce", selectedPlayer1.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO7);
                hideCards(cartaBronce1, cartaBronce2, cartaBronce3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaBronce2:
                insertPlayerOnDB("bronce", selectedPlayer2.getText().toString());
                SobreModelDAO sobreModelDAO8 = new SobreModelDAO("bronce", selectedPlayer2.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO8);
                hideCards(cartaBronce1, cartaBronce2, cartaBronce3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
            case R.id.cartaBronce3:
                insertPlayerOnDB("bronce", selectedPlayer3.getText().toString());
                SobreModelDAO sobreModelDAO9 = new SobreModelDAO("bronce", selectedPlayer3.getText().toString());
                homeViewModel.saveSobre(sobreModelDAO9);
                hideCards(cartaBronce1, cartaBronce2, cartaBronce3, selectedPlayer1, selectedPlayer2, selectedPlayer3);
                showSobres();
                break;
        }
    }

    private void hideSobres() {
        sobreOroImage.setVisibility(View.INVISIBLE);
        sobrePlataImage.setVisibility(View.INVISIBLE);
        sobreBronceImage.setVisibility(View.INVISIBLE);
    }

    private void showSobres() {
        homeText.setText("ABRE UN SOBRE!");

        sobreOroImage.setVisibility(View.VISIBLE);
        sobrePlataImage.setVisibility(View.VISIBLE);
        sobreBronceImage.setVisibility(View.VISIBLE);
    }

    public int getCountPlayers() {
        return countPlayers;
    }

    public int countPlayers=0;//VARIABLE QUE ACUMULA LOS JUGADORES DE TU PLANTILLA
    private void insertPlayerOnDB(String type, String name){
        DatabaseCreatedHelper dbCreated=new DatabaseCreatedHelper(getContext(), "DBCreatedPlayers", null, 1);
        SQLiteDatabase db = dbCreated.getWritableDatabase();
        boolean repeatedCard=false;
        CreatedDBAccess dbAccess = CreatedDBAccess.getInstance(getContext());
        dbAccess.open();
        for (int i = 0; i < dbAccess.getPlayers().size(); i++) {

            if (dbAccess.getPlayers().get(i).equals(name)){
                repeatedCard=true;
            }
            //tv.setText(dbAccess.getPlayers().get(0));

        }
        //pruebagetdatos.setText(dbAccess.getPlayers().get(0));

        dbAccess.close();

        if(!repeatedCard) {
            db.execSQL("INSERT INTO Plantilla(Type, Name) VALUES('" + type + "','" + name + "')");
            countPlayers++;
        }
    }

}