package cat.itb.ui.dashboard;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.R;
import cat.itb.databaseSQLite.CreatedDBAccess;

public class DashboardFragment extends Fragment {

    @BindView(R.id.playercard1)
    ImageView playercard1;
    @BindView(R.id.playercard10)
    ImageView playercard10;
    @BindView(R.id.playercard11)
    ImageView playercard11;
    @BindView(R.id.playercard9)
    ImageView playercard9;
    @BindView(R.id.playercard8)
    ImageView playercard8;
    @BindView(R.id.playercard7)
    ImageView playercard7;
    @BindView(R.id.playercard6)
    ImageView playercard6;
    @BindView(R.id.playercard3)
    ImageView playercard3;
    @BindView(R.id.playercard4)
    ImageView playercard4;
    @BindView(R.id.playercard5)
    ImageView playercard5;
    @BindView(R.id.playercard2)
    ImageView playercard2;


    @BindView(R.id.cromo1)
    ImageView cromo1;
    @BindView(R.id.cromo)
    ImageView cromo;
    @BindView(R.id.cromo2)
    ImageView cromo2;
    @BindView(R.id.cromo3)
    ImageView cromo3;
    @BindView(R.id.cromo4)
    ImageView cromo4;
    @BindView(R.id.cromo5)
    ImageView cromo5;
    @BindView(R.id.cromo9)
    ImageView cromo9;
    @BindView(R.id.cromo7)
    ImageView cromo7;
    @BindView(R.id.cromo8)
    ImageView cromo8;
    @BindView(R.id.cromo6)
    ImageView cromo6;
    @BindView(R.id.cromo10)
    ImageView cromo10;
    @BindView(R.id.cromo11)
    ImageView cromo11;
    @BindView(R.id.cromo12)
    ImageView cromo12;
    @BindView(R.id.cromo13)
    ImageView cromo13;
    @BindView(R.id.cromo14)
    ImageView cromo14;
    @BindView(R.id.cromo15)
    ImageView cromo15;
    @BindView(R.id.cromo19)
    ImageView cromo19;
    @BindView(R.id.cromo18)
    ImageView cromo18;
    @BindView(R.id.cromo17)
    ImageView cromo17;
    @BindView(R.id.cromo16)
    ImageView cromo16;
    @BindView(R.id.cromo20)
    ImageView cromo20;
    @BindView(R.id.cromo22)
    ImageView cromo22;
    @BindView(R.id.cromo23)
    ImageView cromo23;
    @BindView(R.id.cromo24)
    ImageView cromo24;
    @BindView(R.id.cromo21)
    ImageView cromo21;

    @BindView(R.id.nameCromo1)
    TextView nameCromo1;
    @BindView(R.id.nameCromo2)
    TextView nameCromo2;
    @BindView(R.id.nameCromo3)
    TextView nameCromo3;
    @BindView(R.id.nameCromo4)
    TextView nameCromo4;
    @BindView(R.id.nameCromo5)
    TextView nameCromo5;
    @BindView(R.id.nameCromo6)
    TextView nameCromo6;
    @BindView(R.id.nameCromo7)
    TextView nameCromo7;
    @BindView(R.id.nameCromo8)
    TextView nameCromo8;
    @BindView(R.id.nameCromo9)
    TextView nameCromo9;
    @BindView(R.id.nameCromo10)
    TextView nameCromo10;
    @BindView(R.id.nameCromo11)
    TextView nameCromo11;


    @BindView(R.id.goldcard1)
    ImageView goldcard1;
    @BindView(R.id.silvercard1)
    ImageView silvercard1;
    @BindView(R.id.bronzecard1)
    ImageView bronzecard1;
    @BindView(R.id.goldcard10)
    ImageView goldcard10;
    @BindView(R.id.silvercard10)
    ImageView silvercard10;
    @BindView(R.id.bronzecard10)
    ImageView bronzecard10;
    @BindView(R.id.goldcard11)
    ImageView goldcard11;
    @BindView(R.id.silvercard11)
    ImageView silvercard11;
    @BindView(R.id.bronzecard11)
    ImageView bronzecard11;
    @BindView(R.id.goldcard9)
    ImageView goldcard9;
    @BindView(R.id.silvercard9)
    ImageView silvercard9;
    @BindView(R.id.bronzecard9)
    ImageView bronzecard9;
    @BindView(R.id.goldcard8)
    ImageView goldcard8;
    @BindView(R.id.silvercard8)
    ImageView silvercard8;
    @BindView(R.id.bronzecard8)
    ImageView bronzecard8;
    @BindView(R.id.goldcard7)
    ImageView goldcard7;
    @BindView(R.id.silvercard7)
    ImageView silvercard7;
    @BindView(R.id.bronzecard7)
    ImageView bronzecard7;
    @BindView(R.id.goldcard6)
    ImageView goldcard6;
    @BindView(R.id.silvercard6)
    ImageView silvercard6;
    @BindView(R.id.bronzecard6)
    ImageView bronzecard6;
    @BindView(R.id.goldcard3)
    ImageView goldcard3;
    @BindView(R.id.silvercard3)
    ImageView silvercard3;
    @BindView(R.id.bronzecard3)
    ImageView bronzecard3;
    @BindView(R.id.goldcard4)
    ImageView goldcard4;
    @BindView(R.id.silvercard4)
    ImageView silvercard4;
    @BindView(R.id.bronzecard4)
    ImageView bronzecard4;
    @BindView(R.id.goldcard5)
    ImageView goldcard5;
    @BindView(R.id.silvercard5)
    ImageView silvercard5;
    @BindView(R.id.bronzecard5)
    ImageView bronzecard5;
    @BindView(R.id.goldcard2)
    ImageView goldcard2;
    @BindView(R.id.silvercard2)
    ImageView silvercard2;
    @BindView(R.id.bronzecard2)
    ImageView bronzecard2;
    @BindView(R.id.name1)
    TextView name1;
    @BindView(R.id.name2)
    TextView name2;
    @BindView(R.id.name4)
    TextView name4;
    @BindView(R.id.name5)
    TextView name5;
    @BindView(R.id.name3)
    TextView name3;
    @BindView(R.id.name6)
    TextView name6;
    @BindView(R.id.name7)
    TextView name7;
    @BindView(R.id.name8)
    TextView name8;
    @BindView(R.id.name9)
    TextView name9;
    @BindView(R.id.name10)
    TextView name10;
    @BindView(R.id.name11)
    TextView name11;


    private DashboardViewModel dashboardViewModel;
    private Spinner spinner;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        dashboardViewModel =
                ViewModelProviders.of(this).get(DashboardViewModel.class);
        View root = inflater.inflate(R.layout.fragment_dashboard, container, false);
        final TextView textView = root.findViewById(R.id.text_dashboard);
        spinner = root.findViewById(R.id.spinner);
        dashboardViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // playercard11.setImageResource();
        ArrayList<String> formaciones = new ArrayList<>();

        formaciones.add("4-3-3");
        formaciones.add("4-4-2");
        formaciones.add("4-1-4-1");
        formaciones.add("3-4-3");
        formaciones.add("5-3-2");

        ArrayAdapter<CharSequence> arrayAdapter = ArrayAdapter.createFromResource(getContext(), R.array.optionFormaciones, android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                //Implementar cada tipo de formación
                switch (parent.getItemAtPosition(position).toString()) {
                    case ("4-3-3"):
                        Toast.makeText(parent.getContext(), "Formación: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                        setFormacion("4-3-3");
                        break;
                    case ("4-4-2"):
                        Toast.makeText(parent.getContext(), "Formación: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                        setFormacion("4-4-2");
                        break;
                    case ("4-1-4-1"):
                        Toast.makeText(parent.getContext(), "Formación: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                        setFormacion("4-1-4-1");
                        break;
                    case ("3-4-3"):
                        Toast.makeText(parent.getContext(), "Formación: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                        setFormacion("3-4-3");
                        break;
                    case ("5-3-2"):
                        Toast.makeText(parent.getContext(), "Formación: " + parent.getItemAtPosition(position), Toast.LENGTH_SHORT).show();
                        setFormacion("5-3-2");
                        break;

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setFormacion(String formacion) {
        //TODO SWITCH CON LAS POSIBLES FORMACIONES Y QUE MUEVA TODAS LAS POSICIONES
        switch (formacion) {
            case ("4-3-3"):
                ConstraintLayout.LayoutParams params10 = (ConstraintLayout.LayoutParams) playercard10.getLayoutParams();//DC
                params10.setMargins(0, 150, 0, 0);
                playercard10.setLayoutParams(params10);

                ConstraintLayout.LayoutParams params11 = (ConstraintLayout.LayoutParams) playercard11.getLayoutParams();//ED
                params11.setMargins(0, 200, 0, 0);
                playercard11.setLayoutParams(params11);
                //params.setMargins(72,108,0,0);
                ConstraintLayout.LayoutParams params9 = (ConstraintLayout.LayoutParams) playercard9.getLayoutParams();//EI
                params9.setMargins(0, 200, 0, 0);
                playercard9.setLayoutParams(params9);
                //playercard1.setLayoutParams(params);
                ConstraintLayout.LayoutParams params2 = (ConstraintLayout.LayoutParams) playercard2.getLayoutParams();//DFC
                params2.setMargins(0, 950, 0, 0);
                playercard2.setLayoutParams(params2);

                ConstraintLayout.LayoutParams params4 = (ConstraintLayout.LayoutParams) playercard4.getLayoutParams();//DFC
                params4.setMargins(0, 950, 0, 0);
                playercard4.setLayoutParams(params4);

                ConstraintLayout.LayoutParams params3 = (ConstraintLayout.LayoutParams) playercard3.getLayoutParams();
                params3.setMargins(8, 900, 0, 0);
                playercard3.setLayoutParams(params3);

                ConstraintLayout.LayoutParams params5 = (ConstraintLayout.LayoutParams) playercard5.getLayoutParams();
                params5.setMargins(0, 900, 8, 0);
                playercard5.setLayoutParams(params5);

                ConstraintLayout.LayoutParams params6 = (ConstraintLayout.LayoutParams) playercard6.getLayoutParams();
                params6.setMargins(70, 600, 0, 0);
                playercard6.setLayoutParams(params6);

                ConstraintLayout.LayoutParams params7 = (ConstraintLayout.LayoutParams) playercard7.getLayoutParams();
                params7.setMargins(0, 650, 0, 0);
                playercard7.setLayoutParams(params7);

                ConstraintLayout.LayoutParams params8 = (ConstraintLayout.LayoutParams) playercard8.getLayoutParams();
                params8.setMargins(0, 600, 70, 0);
                playercard8.setLayoutParams(params8);
                break;
            case ("4-4-2"):
                ConstraintLayout.LayoutParams params10_2 = (ConstraintLayout.LayoutParams) playercard10.getLayoutParams();//DC
                params10_2.setMargins(250, 150, 0, 0);
                playercard10.setLayoutParams(params10_2);

                ConstraintLayout.LayoutParams params11_2 = (ConstraintLayout.LayoutParams) playercard11.getLayoutParams();//ED
                params11_2.setMargins(430, 500, 0, 0);
                playercard11.setLayoutParams(params11_2);
                //params.setMargins(72,108,0,0);
                ConstraintLayout.LayoutParams params9_2 = (ConstraintLayout.LayoutParams) playercard9.getLayoutParams();//EI
                params9_2.setMargins(250, 190, 0, 0);
                playercard9.setLayoutParams(params9_2);
                //playercard1.setLayoutParams(params);
                ConstraintLayout.LayoutParams params2_2 = (ConstraintLayout.LayoutParams) playercard2.getLayoutParams();//DFC
                params2_2.setMargins(0, 950, 0, 0);
                playercard2.setLayoutParams(params2_2);

                ConstraintLayout.LayoutParams params4_2 = (ConstraintLayout.LayoutParams) playercard4.getLayoutParams();//DFC
                params4_2.setMargins(0, 950, 0, 0);
                playercard4.setLayoutParams(params4_2);

                ConstraintLayout.LayoutParams params3_2 = (ConstraintLayout.LayoutParams) playercard3.getLayoutParams();//LI
                params3_2.setMargins(8, 900, 0, 0);
                playercard3.setLayoutParams(params3_2);

                ConstraintLayout.LayoutParams params5_2 = (ConstraintLayout.LayoutParams) playercard5.getLayoutParams();//LD
                params5_2.setMargins(0, 900, 8, 0);
                playercard5.setLayoutParams(params5_2);

                ConstraintLayout.LayoutParams params6_2 = (ConstraintLayout.LayoutParams) playercard6.getLayoutParams();//MCI
                params6_2.setMargins(30, 500, 30, 0);
                playercard6.setLayoutParams(params6_2);

                ConstraintLayout.LayoutParams params7_2 = (ConstraintLayout.LayoutParams) playercard7.getLayoutParams();//MC
                params7_2.setMargins(0, 650, 150, 0);
                playercard7.setLayoutParams(params7_2);

                ConstraintLayout.LayoutParams params8_2 = (ConstraintLayout.LayoutParams) playercard8.getLayoutParams();//MCD
                params8_2.setMargins(0, 650, 200, 0);
                playercard8.setLayoutParams(params8_2);
                break;
            case ("4-1-4-1"):
                ConstraintLayout.LayoutParams params10_3 = (ConstraintLayout.LayoutParams) playercard10.getLayoutParams();//DC
                params10_3.setMargins(40, 80, 0, 0);
                playercard10.setLayoutParams(params10_3);

                ConstraintLayout.LayoutParams params11_3 = (ConstraintLayout.LayoutParams) playercard11.getLayoutParams();//ED
                params11_3.setMargins(380, 300, 0, 0);
                playercard11.setLayoutParams(params11_3);
                //params.setMargins(72,108,0,0);
                ConstraintLayout.LayoutParams params9_3 = (ConstraintLayout.LayoutParams) playercard9.getLayoutParams();//EI
                params9_3.setMargins(330, 730, 0, 0);
                playercard9.setLayoutParams(params9_3);
                //playercard1.setLayoutParams(params);
                ConstraintLayout.LayoutParams params2_3 = (ConstraintLayout.LayoutParams) playercard2.getLayoutParams();//DFC
                params2_3.setMargins(0, 980, 0, 0);
                playercard2.setLayoutParams(params2_3);

                ConstraintLayout.LayoutParams params4_3 = (ConstraintLayout.LayoutParams) playercard4.getLayoutParams();//DFC
                params4_3.setMargins(0, 980, 0, 0);
                playercard4.setLayoutParams(params4_3);

                ConstraintLayout.LayoutParams params3_3 = (ConstraintLayout.LayoutParams) playercard3.getLayoutParams();//LI
                params3_3.setMargins(8, 900, 0, 0);
                playercard3.setLayoutParams(params3_3);

                ConstraintLayout.LayoutParams params5_3 = (ConstraintLayout.LayoutParams) playercard5.getLayoutParams();//LD
                params5_3.setMargins(0, 900, 8, 0);
                playercard5.setLayoutParams(params5_3);

                ConstraintLayout.LayoutParams params6_3 = (ConstraintLayout.LayoutParams) playercard6.getLayoutParams();//MCI
                params6_3.setMargins(30, 300, 80, 0);
                playercard6.setLayoutParams(params6_3);

                ConstraintLayout.LayoutParams params7_3 = (ConstraintLayout.LayoutParams) playercard7.getLayoutParams(); //MC
                params7_3.setMargins(0, 450, 200, 0);
                playercard7.setLayoutParams(params7_3);

                ConstraintLayout.LayoutParams params8_3 = (ConstraintLayout.LayoutParams) playercard8.getLayoutParams();//MCD
                params8_3.setMargins(0, 450, 250, 0);
                playercard8.setLayoutParams(params8_3);
                break;
            case ("3-4-3"):
                ConstraintLayout.LayoutParams params10_4 = (ConstraintLayout.LayoutParams) playercard10.getLayoutParams();//DC
                params10_4.setMargins(0, 150, 0, 0);
                playercard10.setLayoutParams(params10_4);

                ConstraintLayout.LayoutParams params11_4 = (ConstraintLayout.LayoutParams) playercard11.getLayoutParams();//ED
                params11_4.setMargins(0, 200, 0, 0);
                playercard11.setLayoutParams(params11_4);
                //params.setMargins(72,108,0,0);
                ConstraintLayout.LayoutParams params9_4 = (ConstraintLayout.LayoutParams) playercard9.getLayoutParams();//EI
                params9_4.setMargins(0, 200, 0, 0);
                playercard9.setLayoutParams(params9_4);
                //playercard1.setLayoutParams(params);
                ConstraintLayout.LayoutParams params2_4 = (ConstraintLayout.LayoutParams) playercard2.getLayoutParams();//DFC
                params2_4.setMargins(700, 500, 0, 0);
                playercard2.setLayoutParams(params2_4);

                ConstraintLayout.LayoutParams params4_4 = (ConstraintLayout.LayoutParams) playercard4.getLayoutParams();//DFC
                params4_4.setMargins(0, 950, 210, 0);
                playercard4.setLayoutParams(params4_4);

                ConstraintLayout.LayoutParams params3_4 = (ConstraintLayout.LayoutParams) playercard3.getLayoutParams();//LI
                params3_4.setMargins(8, 900, 0, 0);
                playercard3.setLayoutParams(params3_4);

                ConstraintLayout.LayoutParams params5_4 = (ConstraintLayout.LayoutParams) playercard5.getLayoutParams();//LD
                params5_4.setMargins(0, 900, 8, 0);
                playercard5.setLayoutParams(params5_4);

                ConstraintLayout.LayoutParams params6_4 = (ConstraintLayout.LayoutParams) playercard6.getLayoutParams();//MCI
                params6_4.setMargins(30, 500, 50, 0);
                playercard6.setLayoutParams(params6_4);

                ConstraintLayout.LayoutParams params7_4 = (ConstraintLayout.LayoutParams) playercard7.getLayoutParams();//MC
                params7_4.setMargins(0, 650, 170, 0);
                playercard7.setLayoutParams(params7_4);

                ConstraintLayout.LayoutParams params8_4 = (ConstraintLayout.LayoutParams) playercard8.getLayoutParams();//MCD
                params8_4.setMargins(0, 650, 220, 0);
                playercard8.setLayoutParams(params8_4);
                break;
            case ("5-3-2"):
                ConstraintLayout.LayoutParams params10_5 = (ConstraintLayout.LayoutParams) playercard10.getLayoutParams();//DC
                params10_5.setMargins(0, 950, 0, 0);
                playercard10.setLayoutParams(params10_5);

                ConstraintLayout.LayoutParams params11_5 = (ConstraintLayout.LayoutParams) playercard11.getLayoutParams();//ED
                params11_5.setMargins(0, 200, 200, 0);
                playercard11.setLayoutParams(params11_5);
                //params.setMargins(72,108,0,0);
                ConstraintLayout.LayoutParams params9_5 = (ConstraintLayout.LayoutParams) playercard9.getLayoutParams();//EI
                params9_5.setMargins(200, 200, 0, 0);
                playercard9.setLayoutParams(params9_5);
                //playercard1.setLayoutParams(params);
                ConstraintLayout.LayoutParams params2_5 = (ConstraintLayout.LayoutParams) playercard2.getLayoutParams();//DFC
                params2_5.setMargins(0, 950, 200, 0);
                playercard2.setLayoutParams(params2_5);

                ConstraintLayout.LayoutParams params4_5 = (ConstraintLayout.LayoutParams) playercard4.getLayoutParams();//DFC
                params4_5.setMargins(200, 950, 0, 0);
                playercard4.setLayoutParams(params4_5);

                ConstraintLayout.LayoutParams params3_5 = (ConstraintLayout.LayoutParams) playercard3.getLayoutParams();
                params3_5.setMargins(8, 900, 0, 0);
                playercard3.setLayoutParams(params3_5);

                ConstraintLayout.LayoutParams params5_5 = (ConstraintLayout.LayoutParams) playercard5.getLayoutParams();
                params5_5.setMargins(0, 900, 8, 0);
                playercard5.setLayoutParams(params5_5);

                ConstraintLayout.LayoutParams params6_5 = (ConstraintLayout.LayoutParams) playercard6.getLayoutParams();
                params6_5.setMargins(70, 580, 0, 0);
                playercard6.setLayoutParams(params6_5);

                ConstraintLayout.LayoutParams params7_5 = (ConstraintLayout.LayoutParams) playercard7.getLayoutParams();
                params7_5.setMargins(0, 620, 0, 0);
                playercard7.setLayoutParams(params7_5);

                ConstraintLayout.LayoutParams params8_5 = (ConstraintLayout.LayoutParams) playercard8.getLayoutParams();
                params8_5.setMargins(0, 570, 70, 0);
                playercard8.setLayoutParams(params8_5);
                break;

        }
    }


    @OnClick({R.id.playercard1, R.id.playercard10, R.id.playercard11, R.id.playercard9, R.id.playercard8, R.id.playercard7, R.id.playercard6, R.id.playercard3, R.id.playercard4, R.id.playercard5, R.id.playercard2, R.id.cromo1, R.id.cromo, R.id.cromo2, R.id.cromo3, R.id.cromo4, R.id.cromo5, R.id.cromo9, R.id.cromo7, R.id.cromo8,
            R.id.cromo6, R.id.cromo10, R.id.cromo11, R.id.cromo12, R.id.cromo13, R.id.cromo14, R.id.cromo15, R.id.cromo19, R.id.cromo18, R.id.cromo17, R.id.cromo16, R.id.cromo20, R.id.cromo22, R.id.cromo23, R.id.cromo24, R.id.cromo21})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.playercard1:
                getAllCards();
                setCurrentPlayer(playercard1, name1);

                break;
            case R.id.playercard10:
                getAllCards();
                setCurrentPlayer(playercard10, name10);

                break;
            case R.id.playercard11:
                getAllCards();
                setCurrentPlayer(playercard11, name11);


                break;
            case R.id.playercard9:
                getAllCards();
                setCurrentPlayer(playercard9, name9);


                break;
            case R.id.playercard8:
                getAllCards();
                setCurrentPlayer(playercard8, name8);


                break;
            case R.id.playercard7:
                getAllCards();
                setCurrentPlayer(playercard7, name7);


                break;
            case R.id.playercard6:
                getAllCards();
                setCurrentPlayer(playercard6, name6);


                break;
            case R.id.playercard3:
                getAllCards();
                setCurrentPlayer(playercard3, name3);


                break;
            case R.id.playercard4:
                getAllCards();
                setCurrentPlayer(playercard4, name4);


                break;
            case R.id.playercard5:
                getAllCards();
                setCurrentPlayer(playercard5, name5);


                break;
            case R.id.playercard2:
                getAllCards();
                setCurrentPlayer(playercard2, name2);


                break;
            case R.id.cromo1:
                hideCromos();
                setTeamCard(0);
                showCards();

                break;
            case R.id.cromo:
                hideCromos();
                setTeamCard(1);

                //clickedCromo=cromo;
                //clickedName=nameCromo2;
                showCards();

                break;
            case R.id.cromo2:
                hideCromos();
                setTeamCard(2);

                showCards();

                break;
            case R.id.cromo3:
                hideCromos();
                setTeamCard(3);

                showCards();

                break;
            case R.id.cromo4:
                hideCromos();
                setTeamCard(4);

                showCards();

                break;
            case R.id.cromo5:
                hideCromos();
                setTeamCard(5);

                showCards();

                break;
            case R.id.cromo9:
                hideCromos();
                setTeamCard(9);
                showCards();

                break;
            case R.id.cromo7:
                hideCromos();
                setTeamCard(7);
                showCards();

                break;
            case R.id.cromo8:
                hideCromos();
                setTeamCard(8);
                showCards();

                break;
            case R.id.cromo6:
                hideCromos();
                setTeamCard(6);
                showCards();

                break;
            case R.id.cromo10:
                hideCromos();
                setTeamCard(10);
                showCards();

                break;
            case R.id.cromo11:
                hideCromos();
                showCards();

                break;
            case R.id.cromo12:
                hideCromos();
                clickedCromo=cromo1;
                clickedName=nameCromo1;
                showCards();

                break;
            case R.id.cromo13:
                hideCromos();
                clickedCromo=cromo1;
                clickedName=nameCromo1;
                showCards();

                break;
            case R.id.cromo14:
                hideCromos();
                clickedCromo=cromo1;
                clickedName=nameCromo1;
                showCards();

                break;
            case R.id.cromo15:
                hideCromos();
                clickedCromo=cromo1;
                clickedName=nameCromo1;
                showCards();

                break;
            case R.id.cromo19:
                hideCromos();
                clickedCromo=cromo1;
                clickedName=nameCromo1;
                showCards();

                break;
            case R.id.cromo18:
                hideCromos();
                showCards();

                break;
            case R.id.cromo17:
                hideCromos();
                showCards();

                break;
            case R.id.cromo16:
                hideCromos();
                showCards();

                break;
            case R.id.cromo20:
                hideCromos();
                showCards();

                break;
            case R.id.cromo22:
                hideCromos();
                showCards();

                break;
            case R.id.cromo23:
                hideCromos();
                showCards();

                break;
            case R.id.cromo24:
                hideCromos();
                showCards();

                break;
            case R.id.cromo21:
                hideCromos();
                showCards();
                break;
        }
    }

    private void hideCards() {
        playercard1.setVisibility(View.INVISIBLE);
        playercard2.setVisibility(View.INVISIBLE);
        playercard3.setVisibility(View.INVISIBLE);
        playercard4.setVisibility(View.INVISIBLE);
        playercard5.setVisibility(View.INVISIBLE);
        playercard6.setVisibility(View.INVISIBLE);
        playercard7.setVisibility(View.INVISIBLE);
        playercard8.setVisibility(View.INVISIBLE);
        playercard9.setVisibility(View.INVISIBLE);
        playercard10.setVisibility(View.INVISIBLE);
        playercard11.setVisibility(View.INVISIBLE);
    }

    private void showCards() {
        playercard1.setVisibility(View.VISIBLE);
        playercard2.setVisibility(View.VISIBLE);
        playercard3.setVisibility(View.VISIBLE);
        playercard4.setVisibility(View.VISIBLE);
        playercard5.setVisibility(View.VISIBLE);
        playercard6.setVisibility(View.VISIBLE);
        playercard7.setVisibility(View.VISIBLE);
        playercard8.setVisibility(View.VISIBLE);
        playercard9.setVisibility(View.VISIBLE);
        playercard10.setVisibility(View.VISIBLE);
        playercard11.setVisibility(View.VISIBLE);
    }

    private void hideCromos() {
        cromo.setVisibility(View.INVISIBLE);
        cromo1.setVisibility(View.INVISIBLE);
        cromo2.setVisibility(View.INVISIBLE);
        cromo3.setVisibility(View.INVISIBLE);
        cromo4.setVisibility(View.INVISIBLE);
        cromo5.setVisibility(View.INVISIBLE);
        cromo6.setVisibility(View.INVISIBLE);
        cromo7.setVisibility(View.INVISIBLE);
        cromo8.setVisibility(View.INVISIBLE);
        cromo9.setVisibility(View.INVISIBLE);
        cromo10.setVisibility(View.INVISIBLE);
        cromo11.setVisibility(View.INVISIBLE);
        cromo12.setVisibility(View.INVISIBLE);
        cromo13.setVisibility(View.INVISIBLE);
        cromo14.setVisibility(View.INVISIBLE);
        cromo15.setVisibility(View.INVISIBLE);
        cromo16.setVisibility(View.INVISIBLE);
        cromo17.setVisibility(View.INVISIBLE);
        cromo18.setVisibility(View.INVISIBLE);
        cromo19.setVisibility(View.INVISIBLE);
        cromo20.setVisibility(View.INVISIBLE);
        cromo21.setVisibility(View.INVISIBLE);
        cromo22.setVisibility(View.INVISIBLE);
        cromo23.setVisibility(View.INVISIBLE);
        cromo24.setVisibility(View.INVISIBLE);

        nameCromo1.setVisibility(View.INVISIBLE);
        nameCromo2.setVisibility(View.INVISIBLE);
        nameCromo3.setVisibility(View.INVISIBLE);
        nameCromo4.setVisibility(View.INVISIBLE);
        nameCromo5.setVisibility(View.INVISIBLE);
        nameCromo6.setVisibility(View.INVISIBLE);
        nameCromo7.setVisibility(View.INVISIBLE);
        nameCromo8.setVisibility(View.INVISIBLE);
        nameCromo9.setVisibility(View.INVISIBLE);
        nameCromo10.setVisibility(View.INVISIBLE);
        nameCromo11.setVisibility(View.INVISIBLE);
    }

    private void showCromos() {
        cromo.setVisibility(View.VISIBLE);
        cromo1.setVisibility(View.VISIBLE);
        cromo2.setVisibility(View.VISIBLE);
        cromo3.setVisibility(View.VISIBLE);
        cromo4.setVisibility(View.VISIBLE);
        cromo5.setVisibility(View.VISIBLE);
        cromo6.setVisibility(View.VISIBLE);
        cromo7.setVisibility(View.VISIBLE);
        cromo8.setVisibility(View.VISIBLE);
        cromo9.setVisibility(View.VISIBLE);
        cromo10.setVisibility(View.VISIBLE);
        cromo11.setVisibility(View.VISIBLE);
        cromo12.setVisibility(View.VISIBLE);
        cromo13.setVisibility(View.VISIBLE);
        cromo14.setVisibility(View.VISIBLE);
        cromo15.setVisibility(View.VISIBLE);
        cromo16.setVisibility(View.VISIBLE);
        cromo17.setVisibility(View.VISIBLE);
        cromo18.setVisibility(View.VISIBLE);
        cromo19.setVisibility(View.VISIBLE);
        cromo20.setVisibility(View.VISIBLE);
        cromo21.setVisibility(View.VISIBLE);
        cromo22.setVisibility(View.VISIBLE);
        cromo23.setVisibility(View.VISIBLE);
        cromo24.setVisibility(View.VISIBLE);

        nameCromo1.setVisibility(View.VISIBLE);
        nameCromo2.setVisibility(View.VISIBLE);
        nameCromo3.setVisibility(View.VISIBLE);
        nameCromo4.setVisibility(View.VISIBLE);
        nameCromo5.setVisibility(View.VISIBLE);
        nameCromo6.setVisibility(View.VISIBLE);
        nameCromo7.setVisibility(View.VISIBLE);
        nameCromo8.setVisibility(View.VISIBLE);
        nameCromo9.setVisibility(View.VISIBLE);
        nameCromo10.setVisibility(View.VISIBLE);
        nameCromo11.setVisibility(View.VISIBLE);
    }

    private void setTeamCard(int pos){
        CreatedDBAccess dbAccess = CreatedDBAccess.getInstance(getContext());
        dbAccess.open();
        clickedName.setText(dbAccess.getPlayers().get(pos));
        switch (dbAccess.getTypeCard().get(pos)) {
            case "oro":
                clickedCromo.setImageResource(R.drawable.cartachamp);
                break;
            case "plata":
                clickedCromo.setImageResource(R.drawable.cartaplata);
                break;
            case "bronce":
                clickedCromo.setImageResource(R.drawable.cartabronce);
                break;
        }
        dbAccess.close();
    }

    private void getAllCards() {
        hideCards();
        showCromos();
        CreatedDBAccess dbAccess = CreatedDBAccess.getInstance(getContext());
        dbAccess.open();
        List<ImageView> cardType = new ArrayList<>();
        cardType.add(cromo1);
        cardType.add(cromo);
        cardType.add(cromo2);
        cardType.add(cromo3);
        cardType.add(cromo4);
        cardType.add(cromo5);
        cardType.add(cromo6);
        cardType.add(cromo7);
        cardType.add(cromo8);
        cardType.add(cromo9);
        cardType.add(cromo10);
        cardType.add(cromo14);
        cardType.add(cromo13);
        cardType.add(cromo12);
        cardType.add(cromo11);
        cardType.add(cromo15);
        cardType.add(cromo19);
        cardType.add(cromo18);
        cardType.add(cromo17);
        cardType.add(cromo16);
        cardType.add(cromo20);
        cardType.add(cromo21);
        cardType.add(cromo22);
        cardType.add(cromo23);
        cardType.add(cromo24);

        List<TextView> namesPlayers = new ArrayList<>();
        //namesPlayers.add(nameCromo1.getText().toString());
        namesPlayers.add(nameCromo1);
        namesPlayers.add(nameCromo2);
        namesPlayers.add(nameCromo3);
        namesPlayers.add(nameCromo4);
        namesPlayers.add(nameCromo5);
        namesPlayers.add(nameCromo6);
        namesPlayers.add(nameCromo7);
        namesPlayers.add(nameCromo8);
        namesPlayers.add(nameCromo9);
        namesPlayers.add(nameCromo10);
        namesPlayers.add(nameCromo11);

        for (int i = 0; i < dbAccess.getPlayers().size(); i++) {
            switch (dbAccess.getTypeCard().get(i)) {
                case "oro":
                    cardType.get(i).setImageResource(R.drawable.cartachamp);
                    namesPlayers.get(i).setText(dbAccess.getPlayers().get(i));
                    break;
                case "plata":
                    cardType.get(i).setImageResource(R.drawable.cartaplata);
                    namesPlayers.get(i).setText(dbAccess.getPlayers().get(i));
                    break;
                case "bronce":
                    cardType.get(i).setImageResource(R.drawable.cartabronce);
                    namesPlayers.get(i).setText(dbAccess.getPlayers().get(i));
                    break;
            }

            //tv.setText(dbAccess.getPlayers().get(0));

        }
        //pruebagetdatos.setText(dbAccess.getPlayers().get(0));
        dbAccess.close();

    }

    private ImageView clickedCromo;
    private TextView clickedName;

    private void setCurrentPlayer(ImageView imageView, TextView textView){
        this.clickedCromo=imageView;
        this.clickedName=textView;
    }

}