package cat.itb.databaseSQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class CreatedDBAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static CreatedDBAccess instance;
    Cursor cursor = null;

    private CreatedDBAccess(Context context){
        this.openHelper=new DatabaseCreatedHelper(context,"DBCreatedPlayers", null, 1);
    }

    public static CreatedDBAccess getInstance(Context context){
        if(instance==null){
            instance=new CreatedDBAccess(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getReadableDatabase();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public List<String> getPlayers(){
        ArrayList<String> listPlayers=new ArrayList<>();
        cursor=db.rawQuery("SELECT Name FROM Plantilla", new String[]{});
        //StringBuffer buffer = new StringBuffer();
        while(cursor.moveToNext()){
            String player = cursor.getString(0);
            listPlayers.add(player);
        }
        return  listPlayers;
    }
    public List<String> getTypeCard(){
        ArrayList<String> listTypes=new ArrayList<>();
        cursor=db.rawQuery("SELECT Type FROM Plantilla", new String[]{});
        //StringBuffer buffer = new StringBuffer();
        while(cursor.moveToNext()){
            String type = cursor.getString(0);
            listTypes.add(type);
        }
        return  listTypes;
    }
}
