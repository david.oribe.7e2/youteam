package cat.itb.databaseSQLite;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.ContactsContract;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase db;
    private static DatabaseAccess instance;
    Cursor cursor = null;

    private DatabaseAccess(Context context){
        this.openHelper=new OpenDatabaseHelper(context);
    }

    public static DatabaseAccess getInstance(Context context){
        if(instance==null){
            instance=new DatabaseAccess(context);
        }
        return instance;
    }

    public void open(){
        this.db=openHelper.getWritableDatabase();
    }

    public void close(){
        if(db!=null){
            this.db.close();
        }
    }

    public String getPlayer(int id){
        cursor=db.rawQuery("SELECT player_name FROM Player WHERE id="+id, new String[]{});
        StringBuffer buffer = new StringBuffer();
        while(cursor.moveToNext()){
            String player = cursor.getString(0);
            buffer.append(""+player);
        }
        return  buffer.toString();
    }

}
