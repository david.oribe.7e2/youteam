package cat.itb.databaseSQLite;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

public class OpenDatabaseHelper extends SQLiteAssetHelper {
    private static final String DATABASE_NAME="database.sqlite";
    private static final int DATABASE_VERSION=1;

    public OpenDatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
