package cat.itb.repository;

import android.content.Context;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

import cat.itb.DAO.SobreDAO;
import cat.itb.DAO.db.AppDB;
import cat.itb.model.SobreModelDAO;

public class SobreRepository {
    private SobreDAO dao;
    private LiveData<List<SobreModelDAO>> sobreMD;

    public SobreRepository(Context context) {
        AppDB db = AppDB.getINSTANCE(context);
        this.dao= db.getDAO();
        sobreMD = dao.getSobres();
    }

    public void saveExcursion(SobreModelDAO sobre) { new InsertAsyncTask(dao).execute(sobre); }
    private static class InsertAsyncTask extends AsyncTask<SobreModelDAO, Void, Void> {
        SobreDAO mDao;
        public InsertAsyncTask(SobreDAO dao) {
            mDao=dao;
        }

        @Override
        protected Void doInBackground(SobreModelDAO... sobresMD) {
            mDao.insert(sobresMD[0]);
            return null;
        }
    }

    public LiveData<List<SobreModelDAO>> getSobres() {
        return sobreMD;
    }
}
